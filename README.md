kubectl apply -f . 

Для проверки работоспособности развернутого сервиса: curl -H host=arch.homework localhost:30081/profiles/test

![Проверка работоспособности через nodePort](./img1.png)

Для проверки rewrite "otusapp" на profiles через Ingress:

![rewrite Ingress](./img3.png)

1) Узнать IP адрес
2) curl -H host=arch.homework http://192.168.65.3:30081/otusapp/test

![Проверка работспособности через Ingress](./img2.png)
